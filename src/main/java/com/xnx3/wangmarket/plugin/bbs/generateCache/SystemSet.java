package com.xnx3.wangmarket.plugin.bbs.generateCache;

import org.springframework.stereotype.Component;

import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.generateCache.BaseGenerate;
import com.xnx3.wangmarket.admin.entity.Site;

/**
 * 系统相关
 * @author 管雷鸣
 *
 */
@Component
public class SystemSet extends BaseGenerate{
	
	public SystemSet() {
		isReg();
	}
	
	/**
	 * 生成 isReg的js缓存
	 * @param site
	 */
	public void isReg(){
		createCacheObject("isReg");
		cacheAdd(com.xnx3.wangmarket.plugin.bbs.entity.SystemSet.IS_REG_YES, "允许");
		cacheAdd(com.xnx3.wangmarket.plugin.bbs.entity.SystemSet.IS_REG_NO, "禁止");
		
		this.addCommonJsFunction();
		this.generateCacheFile();
	}
	
}
