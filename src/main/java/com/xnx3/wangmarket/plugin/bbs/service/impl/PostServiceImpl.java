package com.xnx3.wangmarket.plugin.bbs.service.impl;

import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;
import com.xnx3.DateUtil;
import com.xnx3.Lang;
import com.xnx3.StringUtil;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.dao.SqlDAO;
import com.xnx3.j2ee.entity.BaseEntity;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.func.Language;
import com.xnx3.j2ee.func.Safety;
import com.xnx3.j2ee.func.TextFilter;
import com.xnx3.j2ee.shiro.ShiroFunc;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.News;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.entity.PostComment;
import com.xnx3.wangmarket.plugin.bbs.entity.PostData;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import com.xnx3.wangmarket.plugin.bbs.util.Action;
import com.xnx3.wangmarket.plugin.bbs.vo.PostListVO;
import com.xnx3.wangmarket.plugin.bbs.vo.PostVO;

@Service
public class PostServiceImpl implements PostService {
	
	@Resource
	private SqlDAO sqlDAO;

	public SqlDAO getSqlDAO() {
		return sqlDAO;
	}

	public void setSqlDAO(SqlDAO sqlDAO) {
		this.sqlDAO = sqlDAO;
	}

	public BaseVO savePost(HttpServletRequest request, int siteid) {
		BaseVO baseVO = new BaseVO();
		int id = Lang.stringToInt(request.getParameter("id"), 0);
		int classid = Lang.stringToInt(request.getParameter("postClass"), 0);
		String title = Sql.filter(StringUtil.filterXss(request.getParameter("title")));
		String text = request.getParameter("text");		//内容就不经过xss过滤了，因为本身就是html代码，在显示的时候进行防xss
		
		
		if(classid == 0){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_addPostPleaseSelectClass"));
			return baseVO;
		}
		if(title==null || title.length()<Global.bbs_titleMinLength || title.length()>Global.bbs_titleMaxLength){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_addPostTitleSizeFailure").replaceAll("\\$\\{min\\}", Global.bbs_titleMinLength+"").replaceAll("\\$\\{max\\}", Global.bbs_titleMaxLength+""));
			return baseVO;
		}
		if(text==null || text.length()<Global.bbs_textMinLength){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_addPostTextSizeFailure").replaceAll("\\$\\{min\\}",Global.bbs_textMinLength+""));
			return baseVO;
		}
		
		User user = ShiroFunc.getUser();
		
		Post post = null;
		PostData postData = new PostData();
		if(id != 0){
			//修改
			post = sqlDAO.findById(Post.class, id);
			if(post == null){
				baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_updatePostNotFind"));
				return baseVO;
			}
			if(post.getSiteid() - siteid != 0){
				baseVO.setBaseVO(BaseVO.FAILURE, "帖子不属于您！");
				return baseVO;
			}
			
			postData = sqlDAO.findById(PostData.class, post.getId());
			if(postData == null){
				baseVO.setBaseVO(BaseVO.FAILURE, "帖子内容不存在");
				return baseVO;
			}
			
		}else{
			//新增
			post = new Post();
			post.setAddtime(com.xnx3.DateUtil.timeForUnix10());
			post.setUserid(user.getId());
			post.setView(0);
			post.setIsdelete(Post.ISDELETE_NORMAL);
			post.setSiteid(siteid);
			post.setCommentCount(0);
		}
		
		String info="";	//截取简介文字,30字
		String filterText = StringUtil.filterHtmlTag(text);
		if(filterText.length()<60){
			info=filterText;
		}else{
			info=filterText.substring(0,60);
		}
		
		post.setTitle(title);
		post.setClassid(classid);
		post.setInfo(info);
		sqlDAO.save(post);
		
		if(postData.getPostid()==null){
			postData.setPostid(post.getId());
		}
		postData.setText(text);
		
		boolean have = TextFilter.filter(request, "论坛插件帖子发现涉嫌违规："+post.getTitle(), Global.get("MASTER_SITE_URL")+"plugin/bbs/view.do?id="+post.getId(), post.getTitle()+text);
		if(have){
			//涉嫌违规
			post.setLegitimate(Post.LEGITIMATE_NO);
		}else{
			post.setLegitimate(Post.LEGITIMATE_OK);
		}
		
		sqlDAO.save(postData);
		
		if(id != 0){
			//修改
		}else{
			//新增
			Action.addAction(post.getSiteid(), "<a href=\"home.do?siteid="+post.getSiteid()+"&userid="+user.getId()+"\" target=\"_black\">"+user.getNickname()+"</a>发表帖子<a href=\"view.do?id="+post.getId()+"\" target=\"_black\">"+post.getTitle()+"</a>");
		}
		
		
		
		baseVO.setBaseVO(BaseVO.SUCCESS, post.getId()+"");
		return baseVO;
	}

	public BaseVO deletePost(int id) {
		BaseVO baseVO = new BaseVO();
		if(id>0){
			Post p = sqlDAO.findById(Post.class, id);
			if(p!=null){
				p.setIsdelete(BaseEntity.ISDELETE_DELETE);
				sqlDAO.save(p);
			}else{
				baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_deletePostNotFind"));
			}
		}else{
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_deletePostIdFailure"));
		}
		return baseVO;
	}

	public PostVO read(int id) {
		PostVO postVO = new PostVO();
		
		if(id>0){
			//查询帖子详情
			Post post=sqlDAO.findById(Post.class, id);
			if(post == null){
				postVO.setBaseVO(PostVO.FAILURE, Language.show("bbs_viewPostNotFind"));
				return postVO;
			}
			
			//查看帖子所属用户
			User user = sqlDAO.findById(User.class, post.getUserid());
			//检验此用户状态是否正常，是否被冻结
			if(user.getIsfreeze() == User.ISFREEZE_FREEZE){
				postVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_postCreateUserIsFreeze"));
				return postVO;
			}
			postVO.setUser(user);
			
			//查所属板块
			PostClass postClass = sqlDAO.findById(PostClass.class, post.getClassid());
			if(postClass == null || postClass.getIsdelete() == BaseEntity.ISDELETE_DELETE){
				postVO.setBaseVO(PostVO.FAILURE, Language.show("bbs_postViewPostClassIsNotFind"));
			}else{
				postVO.setPostClass(postClass);
				postVO.setPost(post);
				post.setView(post.getView()+1);
				sqlDAO.save(post);
				
				PostData postData = sqlDAO.findById(PostData.class, post.getId());
				postVO.setText(postData.getText());
				
				if(Global.bbs_readPost_addLog){
//					logDAO.insert(post.getId(), "BBS_POST_VIEW", post.getTitle());
				}
			}
		}else{
			postVO.setBaseVO(PostVO.FAILURE, Language.show("bbs_postIdFailure"));
		}
		
		return postVO;
	}

	public int count(int postid) {
		return sqlDAO.count("post_comment", "WHERE postid= "+postid);
	}


	public BaseVO deleteComment(int id) {
		BaseVO baseVO = new BaseVO();
		if(id>0){
			PostComment pc = sqlDAO.findById(PostComment.class, id);
			if(pc!=null){
				pc.setIsdelete(BaseEntity.ISDELETE_DELETE);
				sqlDAO.save(pc);
//				logDAO.insert(pc.getId(), "BBS_POST_DELETE_COMMENT", pc.getText());
			}else{
				baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_deleteCommentNotFind"));
			}
		}else{
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_commentIdFailure"));
		}
		return baseVO;
	}

	public BaseVO addComment(HttpServletRequest request) {
		BaseVO baseVO = new BaseVO();
		int postid = Lang.stringToInt(request.getParameter("postid"), 0);
		String text = request.getParameter("text");
		if(postid == 0){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_unknowCommentPost"));
			return baseVO;
		}
		
		//过滤xss
		text = StringUtil.filterXss(text);
		
		if(text==null || text.length()<2 || text.length()>2000){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_commentSizeFailure").replaceAll("\\$\\{min\\}", 2+"").replaceAll("\\$\\{max\\}", 2000+""));
			return baseVO;
		}
		
		//先查询是不是有这个主贴
		Post p=sqlDAO.findById(Post.class, postid);
		if(p == null){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_commentPostNotFind"));
			return baseVO;
		}
		
//		if(p.getState() != Post.STATE_NORMAL){
//			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_commentPostIsNotNormal"));
//			return baseVO;
//		}
		
		if(p.getIsdelete() == Post.ISDELETE_DELETE){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_commentPostIsDelete"));
			return baseVO;
		}
		
		User user = ShiroFunc.getUser();
				
		PostComment postComment=new PostComment();
		postComment.setPostid(p.getId());
		postComment.setUserid(user.getId());
		postComment.setAddtime(DateUtil.timeForUnix10());
		postComment.setSiteid(p.getSiteid());
		postComment.setText(text);
		postComment.setIsdelete(PostComment.ISDELETE_NORMAL);

		boolean have = TextFilter.filter(request, "论坛插件回帖发现涉嫌违规,回帖用户："+user.getNickname(), Global.get("MASTER_SITE_URL")+"plugin/bbs/view.do?id="+p.getId(), text);
		if(have){
			//涉嫌违规
			postComment.setLegitimate(Post.LEGITIMATE_NO);
		}else{
			postComment.setLegitimate(Post.LEGITIMATE_OK);
		}
		sqlDAO.save(postComment);
		
		//当前帖子回帖数量+1 ， 加入当前回复的时间
		p.setCommentCount(p.getCommentCount()+1);
		p.setLastCommentTime(DateUtil.timeForUnix10());
		sqlDAO.save(p);
		
		//动作
		Action.addAction(p.getSiteid(), "<a href=\"home.do?siteid="+p.getSiteid()+"&userid="+user.getId()+"\" target=\"_black\">"+user.getNickname()+"</a>回复了帖子<a href=\"view.do?id="+p.getId()+"\" target=\"_black\">"+p.getTitle()+"</a>");
//		sqlDAO.addOne("plugin_bbs_post", "comment_count", "id="+postComment.getPostid());
		
//		logDAO.insert(postComment.getId(), "BBS_POST_COMMENT_ADD", StringUtil.filterHtmlTag(postComment.getText()));
		return baseVO;
	}

	public List commentAndUser(int postid) {
		return commentAndUser(postid, 0);
	}

	public List commentAndUser(int postid, int limit) {
		String limitString="";
		if(limit > 0){
			limitString = " LIMIT 0,"+limit;
		}
		
		return sqlDAO.findMapBySqlQuery("SELECT comment.addtime,comment.userid,comment.text,user.head,user.nickname,user.id FROM plugin_bbs_post_comment comment,user WHERE comment.userid=user.id AND comment.postid= "+postid+" AND user.isfreeze="+User.ISFREEZE_NORMAL+" ORDER BY comment.id DESC "+limitString);
	}
	
	public BaseVO savePostClass(HttpServletRequest request, int siteid) {
		BaseVO baseVO = new BaseVO();
		int id = Lang.stringToInt(request.getParameter("id"), 0);
		String name = request.getParameter("name");
		name = Safety.filter(name);
		if(name == null || name.length()==0){
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_savePostClassNameNotNull"));
			return baseVO;
		}
		
		PostClass postClass = null;
		if(id==0){	//新增
			postClass = new PostClass();
			postClass.setSiteid(siteid);
		}else{
			//修改
			postClass = sqlDAO.findById(PostClass.class, id);
			if(postClass == null){
				baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_updatePostClassNotFind"));
				return baseVO;
			}
			//判断归属
			if(postClass.getSiteid() - siteid != 0){
				baseVO.setBaseVO(BaseVO.FAILURE, "板块不属于你，无法操作");
				return baseVO;
			}
		}
		
		postClass.setName(name);
		postClass.setIsdelete(BaseEntity.ISDELETE_NORMAL);
		
		sqlDAO.save(postClass);
		if((id==0 && postClass.getId()>0) || id > 0){
			if(id>0){
//				logDAO.insert(postClass.getId(), "ADMIN_SYSTEM_BBS_CLASS_SAVE", postClass.getName());
			}else{
//				logDAO.insert(postClass.getId(), "ADMIN_SYSTEM_BBS_CLASS_ADD", postClass.getName());
			}
		}else{
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_savePostClassFailure"));
		}
		return baseVO;
	}

	public BaseVO deletePostClass(int id) {
		BaseVO baseVO = new BaseVO();
		if(id>0){
			PostClass pc = sqlDAO.findById(PostClass.class, id);
			if(pc!=null){
				pc.setIsdelete(BaseEntity.ISDELETE_DELETE);
				sqlDAO.save(pc);
//				logDAO.insert(pc.getId(), "ADMIN_SYSTEM_BBS_POST_DELETE", pc.getName());
			}else{
				baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_deletePostClassNotFind"));
			}
		}else{
			baseVO.setBaseVO(BaseVO.FAILURE, Language.show("bbs_pleaseAddDeletePostClassID"));
		}
		return baseVO;
	}
	
	public PostListVO weekHot(String siteid, String classid){
		return weekHot(Lang.stringToInt(siteid, 0), Lang.stringToInt(classid, 0));
	}
	
	public PostListVO weekHot(int siteid, int classid){
		PostListVO vo = new PostListVO();
		if(siteid < 1){
			vo.setBaseVO(BaseVO.FAILURE, "请传入站点");
			return vo;
		}
		
		String sql = "SELECT * FROM plugin_bbs_post WHERE addtime > "+ (DateUtil.timeForUnix10() - (1*60*60*24*7)) +" AND siteid = "+siteid;
		if(classid > 0){
			sql = sql + " AND classid = "+classid;
		}
		sql = sql + " ORDER BY comment_count DESC LIMIT 10";
		List<Post> list = sqlDAO.findBySqlQuery(sql, Post.class);
		vo.setList(list);
		
		return vo;
	}
}
