package com.xnx3.wangmarket.plugin.bbs.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.taglibs.standard.lang.jstl.test.beans.PublicBean1;

/**
 * 论坛的系统设置
 * @author 管雷鸣
 *
 */
@Entity
@Table(name = "plugin_bbs_system_set")
public class SystemSet implements java.io.Serializable {
	
	/**
	 * 是否允许注册，1，允许
	 */
	public final static Short IS_REG_YES = 1;
	/**
	 * 是否允许注册，0，禁止
	 */
	public final static Short IS_REG_NO = 0;
	

	private Integer id;
	private String name; //论坛名字
	private Short isReg; //是否允许注册


	// Property accessors
	@Id
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "name", columnDefinition="char(100) COLLATE utf8mb4_unicode_ci COMMENT '论坛名字'")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public Short getIsReg() {
		return isReg;
	}

	public void setIsReg(Short isReg) {
		this.isReg = isReg;
	}

	@Override
	public String toString() {
		return "SystemSet [id=" + id + ", name=" + name + ", isReg=" + isReg + "]";
	}
	

}