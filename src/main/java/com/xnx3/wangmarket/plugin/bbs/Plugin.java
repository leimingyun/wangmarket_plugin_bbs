package com.xnx3.wangmarket.plugin.bbs;

import java.util.Arrays;

import com.xnx3.wangmarket.admin.pluginManage.anno.PluginRegister;

/**
 * 论坛，应用于网站后台
 * @author 管雷鸣
 */
@PluginRegister(id="bbs" , menuTitle = "论坛", menuHref="../../plugin/bbs/admin/index.do", applyToCMS=true, intro="论坛功能插件，让每个网站都有一个自己的独立论坛。", version="1.1", versionMin="5.7")
public class Plugin{
	
}