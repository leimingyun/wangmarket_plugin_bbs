package com.xnx3.wangmarket.plugin.bbs.vo;

import java.util.List;
import java.util.Map;

import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.entity.PostComment;

/**
 * 回帖列表
 * @author 管雷鸣
 *
 */
public class PostCommentListVO extends BaseVO{
	private List<Map<String,Object>> list;
	Page page;
	
	public List<Map<String, Object>> getList() {
		return list;
	}
	public void setList(List<Map<String, Object>> list) {
		this.list = list;
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	@Override
	public String toString() {
		return "PostCommentListVO [list=" + list + ", page=" + page + "]";
	}
	
}
