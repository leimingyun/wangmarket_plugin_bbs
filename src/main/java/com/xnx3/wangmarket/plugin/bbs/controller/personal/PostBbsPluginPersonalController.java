package com.xnx3.wangmarket.plugin.bbs.controller.personal;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.Lang;
import com.xnx3.j2ee.entity.User;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.func.Captcha;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.j2ee.vo.UploadFileVO;
import com.xnx3.wangmarket.admin.util.AliyunLog;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;
import com.xnx3.wangmarket.plugin.bbs.controller.BBSBaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;
import net.sf.json.JSONObject;

/**
 * 论坛，帖子相关，用户登陆后的操作
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/")
public class PostBbsPluginPersonalController extends BBSBaseController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;
	@Resource
	private UserService userService;
	
	

	/**
	 * 发帖
	 * @param classid 要发表到哪个分类下(论坛板块)
	 */
	@RequestMapping("addPost${url.suffix}")
	public String addPost(Model model, HttpServletRequest request){
		User user = getUser();
		String classid = getRequestClassid(request);
		if(user == null){
			return error(model, "请先登陆", "plugin/bbs/login.do?siteid="+getRequestSiteid(request)+"&classid="+classid);
		}
		
		AliyunLog.addActionLog(classid, "进入论坛发帖页面");
		
		model.addAttribute("classid", classid);
		modelSet(request, model);
		return "plugin/bbs/post/addPost";
	}
	
	/**
	 * 发帖提交页面
	 * @param text 帖子内容
	 */
	@RequestMapping(value="addPostSubmit${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO addPostSubmit(HttpServletRequest request, Model model){
		User user = getUser();
		if(user == null){
			return error("请先登陆");
		}
		
		//验证码校验
		BaseVO capVO = Captcha.compare(request.getParameter("code"), request);
		if(capVO.getResult() == BaseVO.FAILURE){
			AliyunLog.addActionLog(0, "发帖时，验证码错误！"+filter(request.getParameter("code")));
			return capVO;
		}
		
		int classid = Lang.stringToInt(request.getParameter("postClass"), 0);
		if(classid == 0){
			return error("请选择发布板块");
		}
		PostClass postClass = sqlService.findById(PostClass.class, classid);
		if(postClass == null){
			return error("发布的板块不存在");
		}
		
		BaseVO baseVO = postService.savePost(request, postClass.getSiteid());
		if(baseVO.getResult() == BaseVO.SUCCESS){
			ActionLogCache.insert(request, Lang.stringToInt(baseVO.getInfo(), 0), "帖子发布成功");
			return baseVO;
		}else{
			ActionLogCache.insert(request, "帖子发布失败", baseVO.getInfo());
			return error(baseVO.getInfo());
		}
	}
	
	
	/**
	 * 回帖处理
	 * @param post {@link Post}
	 * @param text 回帖内容
	 */
	@RequestMapping("addCommentSubmit${url.suffix}")
	@ResponseBody
	public BaseVO commentSubmit(HttpServletRequest request,Model model){
		User user = getUser();
		if(user == null){
			return error("请先登陆");
		}
		
		//验证码校验
		BaseVO capVO = Captcha.compare(request.getParameter("code"), request);
		if(capVO.getResult() == BaseVO.FAILURE){
			AliyunLog.addActionLog(0, "回帖时，验证码错误！"+filter(request.getParameter("code")));
			return capVO;
		}
		
		BaseVO baseVO = postService.addComment(request);
		if(baseVO.getResult() == BaseVO.SUCCESS){
			//回帖成功
			ActionLogCache.insert(request, "回帖");
		}else{
			ActionLogCache.insert(request, "回帖", "出错："+baseVO.getInfo());
		}
		return baseVO;
	}
	
	

	/**
	 * 帖子编辑器上传图片接口
	 */
	@RequestMapping(value="uploadImage${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public String uploadImage(Model model,HttpServletRequest request){
		JSONObject json = new JSONObject();
		User user = getUser();
		if(user == null){
			json.put("code", -1);
			json.put("msg", "请先登陆");
			return json.toString();
		}
		
		
		UploadFileVO uploadFileVO = AttachmentFile.uploadImage("site/"+getSiteId()+"/plugin_bbs/", request, "file", 0);
		
		if(uploadFileVO.getResult() == UploadFileVO.SUCCESS){
			//上传成功，写日志
			AliyunLog.addActionLog(getSiteId(), "论坛插件上传图片成功："+uploadFileVO.getFileName(), uploadFileVO.getPath());
			
			json.put("code", 0);
			json.put("msg", "成功");
			JSONObject dataJson = new JSONObject();
			dataJson.put("src", uploadFileVO.getUrl());
			dataJson.put("title", uploadFileVO.getFileName());
			json.put("data", dataJson);
		}else{
			json.put("code", -1);
			json.put("msg", uploadFileVO.getInfo());
		}
		
		return json.toString();
	}
	
}
