package com.xnx3.wangmarket.plugin.bbs.controller.admin;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.xnx3.j2ee.Global;
import com.xnx3.j2ee.entity.BaseEntity;
import com.xnx3.j2ee.func.ActionLogCache;
import com.xnx3.j2ee.func.AttachmentFile;
import com.xnx3.j2ee.service.SqlService;
import com.xnx3.j2ee.service.UserService;
import com.xnx3.j2ee.util.Page;
import com.xnx3.j2ee.util.Sql;
import com.xnx3.j2ee.vo.BaseVO;
import com.xnx3.wangmarket.admin.entity.Site;
import com.xnx3.wangmarket.admin.util.AliyunLog;
import com.xnx3.wangmarket.plugin.base.controller.BasePluginController;
import com.xnx3.wangmarket.plugin.bbs.controller.BBSBaseController;
import com.xnx3.wangmarket.plugin.bbs.entity.Post;
import com.xnx3.wangmarket.plugin.bbs.entity.PostClass;
import com.xnx3.wangmarket.plugin.bbs.entity.PostComment;
import com.xnx3.wangmarket.plugin.bbs.entity.SystemSet;
import com.xnx3.wangmarket.plugin.bbs.generateCache.Bbs;
import com.xnx3.wangmarket.plugin.bbs.service.PostService;

import cn.zvo.fileupload.framework.springboot.FileUploadUtil;
import net.sf.json.JSONObject;

/**
 * 论坛，系统设置
 * @author 管雷鸣
 */
@Controller
@RequestMapping("/plugin/bbs/admin/system/")
public class SystemBbsPluginAdminController extends BBSBaseController {
	@Resource
	private PostService postService;
	@Resource
	private SqlService sqlService;

	/**
	 * 系统设置首页
	 */
	@RequestMapping("index${url.suffix}")
	public String index(HttpServletRequest request,Model model){
		if(!haveSiteAuth()){
			return error(model, "无权限");
		}
		Site site = getSite();
		
		SystemSet set = sqlService.findById(SystemSet.class, site.getId());
		if(set == null){
			//第一次用，设置不存在，那么初始化创建一个
			set = new SystemSet();
			set.setId(site.getId());
			set.setName("论坛名");
			set.setIsReg(SystemSet.IS_REG_YES); 	//默认允许注册
			sqlService.save(set);
			AliyunLog.addActionLog(set.getId(), "论坛插件管理后台系统设置页面", "第一次用，创建初始数据");
		}
		
		model.addAttribute("systemSet", set);
		model.addAttribute("site", site);
		modelSet(request, model);
		return "/plugin/bbs/admin/system/index";
	}
	
	/**
	 * 保存设置项。保存后会自动刷新文件缓存的json数据
	 * @param name 数据表的列名
	 * @param value 值
	 * @return
	 */
	@RequestMapping(value="save${url.suffix}", method = RequestMethod.POST)
	@ResponseBody
	public BaseVO save(HttpServletRequest request,
			@RequestParam(value = "name", required = false , defaultValue="") String name,
			@RequestParam(value = "value", required = false , defaultValue="") String value){
		name = filter(name);
		Site site = getSite();
		
		//可允许修改的 name 
		String columnName = ",name,is_reg,";
		if(columnName.indexOf(","+name+",") == -1){
			return error("name不再可允许修改的范围");
		}
		//更新
		sqlService.executeSql("UPDATE plugin_bbs_system_set SET "+name+" = '"+filter(value)+"' WHERE id = "+site.getId());
		//获取最新的
		SystemSet ss = sqlService.findById(SystemSet.class, site.getId());
		
		//刷新重新生成缓存js文件
		JSONObject json = JSONObject.fromObject(ss);
		generateSystemSetJS(site.getId(), json);
		
		return success();
	}
	
	/**
	 * 创建 plugin_bbs_system_set.js 缓存文件
	 * @param siteid 论坛所属的站点编号
	 * @param json 
	 */
	private void generateSystemSetJS(int siteid, JSONObject json){
		StringBuffer sb = new StringBuffer();
		sb.append("var bbs = new Array();");
		
		//然后用Iterator迭代器遍历取值，建议用反射机制解析到封装好的对象中
		Iterator iterator = json.keys();
		while(iterator.hasNext()){
			String key = (String) iterator.next();
			String value = json.getString(key).replaceAll("\"", "\\\\\"");
			sb.append(" bbs['"+key+"'] = \""+value+"\"; ");
		}
		
		String text = sb.toString();
		
		//将js后缀也加入可上传
//		String suffix[] = FileUploadUtil.fileupload.getAllowUploadSuffixs();
//		String newSuffix[] = new String[suffix.length+1];
//		System.arraycopy(suffix, 0, newSuffix, 0, suffix.length);
//		newSuffix[suffix.length] = "js";
//		FileUploadUtil.fileupload.setAllowUploadSuffixs(newSuffix);
		
		FileUploadUtil.uploadString("site/"+siteid+"/data/plugin_bbs_system_set.js", text);
	}
	
}
