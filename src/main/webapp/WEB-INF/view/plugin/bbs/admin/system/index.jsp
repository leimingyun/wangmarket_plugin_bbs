<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../../../../iw/common/head.jsp">
	<jsp:param name="title" value="文章列表"/>
</jsp:include>
<link href="/css/site_two_subMenu.css" rel="stylesheet">
<script src="/<%=Global.CACHE_FILE %>SystemSet_isReg.js"></script>


<div style="padding:10%;">
	<table class="layui-table" >
	  <tbody>
	    <tr>
	      <td style="width:120px;">论坛名字</td>
	      <td onclick="updateName();" style="cursor:pointer;">
	      	${systemSet.name }
	      	<i class="layui-icon layui-icon-edit" style="padding-left:10px;"></i>	
	      </td>
	    </tr>
	    <tr>
	      <td>是否开放注册</td>
	      <td onclick="upIsReg();" style="cursor:pointer;">
	      	<script type="text/javascript">document.write(isReg['${systemSet.isReg }']);</script>
	      	<i class="layui-icon layui-icon-edit" style="padding-left:10px;"></i>	
	      </td>
	    </tr>
	  </tbody>
	</table>
</div>

<script>

//更改昵称
function updateName(){
	layer.prompt({
	  formType: 0,
	  value: '${systemSet.name}',
	  title: '修改论坛名字'
	}, function(value, index, elem){
		layer.close(index);
		update("name",value);
	});
}

//修改是否允许注册
function upIsReg(){
	layer.confirm('是否允许论坛用户自行注册？', {
	  btn: ['允许', '禁止'] //可以无限个按钮
	}, function(index, layero){
		layer.close(index);
		update("is_reg",'1');
	}, function(index){
		layer.close(index);
	  	update("is_reg",'0');
	});
}

//name:数据表的列名，value：这一列要修改成什么
function update(name, value){
	parent.iw.loading("修改中");
	$.post("save.do", {"name":name ,"value":value }, function(data){
		 	parent.iw.loadClose();
		 	if(data.result == '1'){
		 		parent.iw.msgSuccess("修改成功");
		 		location.reload();
		  	}else if(data.result == '0'){
		  		parent.iw.msgFailure(data.info);
		  	}else{
		  		parent.iw.msgFailure();
		  	}
	    }, 
	"json");
}
</script>

</body>
</html>