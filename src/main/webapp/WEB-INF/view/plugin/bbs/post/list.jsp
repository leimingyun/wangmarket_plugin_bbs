<%@page import="com.xnx3.net.OSSUtil"%>
<%@page import="com.xnx3.j2ee.Global"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<%@ taglib uri="http://www.xnx3.com/java_xnx3/xnx3_tld" prefix="x" %>
<jsp:include page="../common/head.jsp">
   	<jsp:param name="title" value="帖子列表"/>
   	<jsp:param name="keywords" value=""/>
   	<jsp:param name="description" value=""/>
</jsp:include>
<script type="text/javascript" src="${AttachmentFileNetUrl }site/${siteid }/data/plugin_bbs_postClass.js"></script>
<link rel="stylesheet" href="/plugin/bbs/css/global.css">


<div>&nbsp;</div>
<div class="layui-container">
  <div class="layui-row layui-col-space15">
    <div class="layui-col-md8">
      <div class="fly-panel" style="margin-bottom: 0;">
        
        <div class="fly-panel-title fly-filter">
        	
        	<a id="class_0" href="list.do?siteid=${siteid }">所有帖子</a>
        	<span class="fly-mid"></span>
        	
        	<script>
				try{
					if(typeof(postClass) == 'undefined'){
						var postClass = new Array();
						postClass['0'] = '请在管理后台添加论坛板块';
						alert('请在论坛的管理后台，添加上论坛板块。');
					}
					for (var p in postClass) {
				        document.write('<a id="class_'+p+'" href="list.do?siteid=${siteid }&classid='+p+'">'+postClass[p]+'</a><span class="fly-mid"></span>');
				    }
				    document.getElementById('class_<%=request.getParameter("classid")==null? "0":request.getParameter("classid") %>').setAttribute("class", "layui-this");
				}catch(e){
					console.log(e);
				}
			</script>  
        
          
          <style>
          	.layui-form{
          		float:right;
          		padding-top: 6px;
          		margin-right: -40px;
          	}
          	.layui-unselect{
          		border-width:0px;
          		width: 100px;
          	}
          	.fly-filter-right>.layui-form{
          		width:100px;
          	}
        </style>
          <span class="fly-filter-right layui-hide-xs" style="right: 0px;">
	          <script type="text/javascript"> orderBy('id_DESC=发布时间,last_comment_time_DESC=最近回复,view_DESC=阅读最多'); </script>
          </span>
          
        </div>

        <ul class="fly-list">  
        
        
        
<c:forEach items="${list}" var="post">
          <li>
            <a href="home.do?siteid=${siteid }&userid=${post['userid'] }" class="fly-avatar">
              <img src="${post['head'] }" alt="${post['nickname'] }" onerror="this.src='http://res.weiunity.com/image/default_head.png'" style="border-radius: 30px;" />
            </a>
            <h2>
              <a href="view.do?id=${post['id'] }"><x:substring maxLength="20" text="${post['title'] }"></x:substring></a>
            </h2>
            <div class="fly-list-info">
              <a href="home.do?siteid=${siteid }&userid=${post['userid'] }">
                <cite>${post['nickname'] }</cite>
              </a>
              <span><x:time linuxTime="${post['addtime'] }" format="yy-MM-dd HH:mm"></x:time></span>
              <span class="fly-list-nums"> 
                <i class="iconfont" title="阅读次数">&#xe60b;</i> ${post['view'] }
                <i class="iconfont icon-pinglun1" title="回复数量"></i> ${post['comment_count'] }
              </span>
            </div>
          </li>

</c:forEach>        
        
              
          
          
          
        </ul>
        
        <!-- <div class="fly-none">没有相关数据</div> -->
    
        <div style="text-align: center">
        	<jsp:include page="../common/page.jsp"></jsp:include>
        </div>

      </div>
    </div>
    <div class="layui-col-md4">
    	<jsp:include page="../common/right.jsp"></jsp:include> 
    </div>
  </div>
</div>


				
<jsp:include page="../common/foot.jsp"></jsp:include> 