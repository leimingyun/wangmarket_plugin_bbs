
## 功能介绍
[wangmarket（网市场云建站系统）](https://gitee.com/mail_osc/wangmarket) 的 论坛插件。让每个网站都有一个自己的、独立的论坛。
  

## 使用条件
1. 本项目(生成的 target/wangmarket-plugin-xxx.jar)放到 [网市场云建站系统](https://gitee.com/leimingyun/wangmarket_deploy) 中才可运行使用
1. 网市场云建站系统本身需要 v5.7 或以上版本。
1. 本项目只支持 mysql 数据库。使用默认 sqlite 数据库的不可用（需要标注一下支持哪种数据库。如果是这个插件涉及到数据表、以及数据表字段方面的改动，那就只支持mysql数据库。如果没有数据表及字段属性的改动，那就支持 mysql、sqlite数据库）

## 使用方式
1. 下载 /target/ 中的jar包
1. 将下载的jar包放到 tomcat/webapps/ROOT/WEB-INF/lib/ 下
1. 将 tomcat/webapps/ROOT/WEB-INF/classes/application.properties 中，增加配置，注意，主要是要将js这个后缀加入，不然使用会提示请配置论坛版块，就是找不到js缓存导致的

````
fileupload.allowUploadSuffix=js|png|jpg|jpeg|gif|bmp|flv|swf|mkv|avi|rm|rmvb|mpeg|mpg|ogg|ogv|mov|wmv|mp4|webm|mp3|wav|mid|rar|zip|tar|gz|7z|bz2|cab|iso|doc|docx|xls|xlsx|ppt|pptx|pdf|txt|md|xml
````

1. 重新启动运行项目，登陆网站管理后台，即可看到左侧的 功能插件 下多了此功能。


## 二次开发
#### 本插件的二次开发
1. 运行项目，随便登陆个网站管理后台（默认运行起来后，会自带一个默认账号密码都是 wangzhan 的账号，可以用此账号登陆）
1. 登陆网站管理后台后，找到左侧的 功能插件 下的 论坛 ，即可。

#### 从头开始开发一个自己的插件
参考文档：  https://gitee.com/leimingyun/dashboard/wikis/leimingyun/wm/preview?sort_id=3213258&doc_id=1101390

#### 二次开发wangmarket及功能的扩展定制
可参考：  https://gitee.com/leimingyun/wangmarket_deploy

## 合作洽谈
作者：管雷鸣<br/>
微信：xnx3com<br/>
QQ：921153866<br/>
